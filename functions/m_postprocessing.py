"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft
from typing import Tuple

def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
  #  if len(x) == len(y) == len(z):
     #   acc_x_squared = np.square(x)
      #  acc_y_squared = np.square(y)
      #  acc_z_squared = np.square(z)
        
 #   else:
     #   shortest = min(len(x), len(y), len(z))
     #   acc_x_squared = np.square(x[0:shortest])
     #   acc_y_squared = np.square(y[0:shortest])
     #   acc_z_squared = np.square(z[0:shortest])
    x_acc_sq = np.square(x)
    y_acc_sq = np.square(y)
    z_acc_sq = np.square(z)
    
    abs_acc = np.sqrt(x_acc_sq + y_acc_sq + z_acc_sq)

    return abs_acc

def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """
    start = min(time)
    end = max(time)
    equi_time = np.linspace(start,end,len(time))
    interp_data = np.interp(equi_time,xp=time,fp=data)
    return equi_time, interp_data

def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    x -= np.mean(x)
    
    #Berechnen der FFT
    fft_data = fft(x)
    
    #Frequenzen ermitteln
    timesteps = time[1] - time[0]
    sampling_rate = 1/timesteps
    frequencies = np.fft.fftfreq(len(x), d=1/sampling_rate)
    
    #Nur positive Frequenzen filtern, da negative Frequenzen keinen Sinn machen 
    pos_frequencies = frequencies >= 0
   
    return fft_data[pos_frequencies], frequencies[pos_frequencies]
    
     




    